## Amazon Virtual Private Cloud (VPC)
- Amazon VPC lets you provision a logically isolated section of the AWS cloud where you can launch AWS resources in a virtual network that you define. 
- You have complete control over your virtual networking environment, including 
	- selection of your own IP address range
	- creation of subnets
	- configuration of route tables and network gateways.  
- You can use both IPv4 and IPv6 in your VPC for secure and easy access to resources and applications.


### [Create an Amazon VPC Using the VPC Wizard.](https://www.qwiklabs.com/focuses/279?catalog_rank=%7B%22rank%22%3A1%2C%22num_filters%22%3A0%2C%22has_search%22%3Atrue%7D&locale=zh_TW&parent=catalog&search_id=5353213)
- Public and private subnets
- Route tables and routes
- NAT Gateways
- Network ACLs
- Elastic IPs

#### Create an Elastic IP address
Your VPC will launch a NAT Gateway to provide Internet access to private resources. The NAT Gateway will be assigned a static IP address, known as an **Elastic IP address**
- An Elastic IP address is a public IPv4 address, which is reachable from the Internet. 
- It is a static IP address and the IP address will not change. 
- You can associate the Elastic IP address with a resource in your VPC, such as a NAT Gateway or an Amazon EC2 instance. 
- You retain control of the Elastic IP address until you release it back to AWS.

#### Create an Amazon VPC (VPC with Public and Private Subnets)
- In this task you will create an Amazon VPC using the **VPC wizard**. 
- The wizard automatically creates a VPC based upon parameters you specify. 
- Using the VPC Wizard is much simpler than manually creating each component of the VPC.


Here is an overview of the VPC you will create:

![VPC with Public and Private](images/overview.png)


- Click **VPC Dashboard in the top-left corner.
- Click **Launch VPC Wizard**
- The wizard offers four pre-defined configurations. Click each option in the Wizard to view their definition:
	- **VPC with a Single Public Subnet:** A single public subnet connected to the Internet. This is ideal for applications that operate purely in the AWS cloud.
	- **VPC with Public and Private Subnets:** A public subnet for Internet-facing resources and a private subnet for back-end resources. A NAT Gateway is also launched to provide Internet access for resources in the private subnet. This is ideal for keeping private resources separate from the Internet.
	- **VPC with Public and Private Subnets and Hardware VPN Access:** A public subnet and a private subnet, plus a Virtual Private Network (VPN) connection to an existing Corporate Data Center. This is ideal when you have legacy infrastructure in a data center, which can connect to the AWS cloud as a combined network.
	- **VPC with a Private Subnet Only and Hardware VPN Access:** A private subnet connected to a Corporate Data Center via a VPN connection. This is ideal for *bursting into the AWS cloud* to provide additional resources while remaining totally secure from Internet access. This design is often used for Development and Testing, where no direct Internet access is required.

- Click **VPC with Public and Private Subnets** 
