## Amazon Simple Queue Service (SQS)
![SQS](images/Queue_HLD.png)
- **Amazon SQS** offers a secure, durable, and available hosted queue that lets you integrate and decouple distributed software systems and components. 
- **Amazon SQS** offers common constructs such as dead-letter queues and cost allocation tags. 
- It provides a generic web services API and it can be accessed by any programming language that the AWS SDK supports.

### Amazon SQS supports two type of queue:
- standard queue:
![SQS](images/standard-queue.png)
    - default queue type, send data between applications when the throughput is important
    - support a nearly unlimited number of API calls per second
    - support at-least-once message delivery 
    - occasionally more than one copy of a message might be delivered out of order. 
    - Best-Effort Ordering which ensures that messages are generally delivered in the same order as they're sent.
- [FIFO queue](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/FIFO-queues.html):
![SQS](images/fifo-queue.png)
    - Exactly-once processing but have a limited number of transactions per second (TPS)
    - designed to enhance messaging between applications when the order of operations and events is critical, or where duplicates can't be tolerated

### How is Amazon SQS different from Amazon MQ or Amazon SNS?
- Amazon SQS and Amazon SNS are queue and topic services that are highly scalable, simple to use, and don't require you to set up message brokers. We recommend these services for new applications that can benefit from nearly unlimited scalability and simple APIs.
- Amazon MQ is a managed message broker service that provides compatibility with many popular message brokers. We recommend Amazon MQ for migrating applications from existing message brokers that rely on compatibility with APIs such as JMS or protocols such as AMQP, MQTT, OpenWire, and STOMP.


### [Creating an Amazon SQS queue](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-create-queue.html)
- AWS web console
    - Choose Create New Queue.
    - On the Create New Queue page, ensure that you're in the correct region and then type the Queue Name.
- AWS JAVA SDK
```java
// Create a standard queue
System.out.println("Creating a new SQS queue called MyQueue.\n");
final CreateQueueRequest createQueueRequest = new CreateQueueRequest("MyQueue");
final String myQueueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();
```

### Sending a message to the queue
- AWS web console
    - Queue Actions, select Send a Message
- AWS JAVA SDK
```java
// Send a message
System.out.println("Sending a message to MyQueue.\n");
sqs.sendMessage(new SendMessageRequest(myQueueUrl, "This is my message text."));
```

### Listing all Amazon SQS queues in a Region
```java
// List queues
System.out.println("Listing all queues in your account.\n");
for (final String queueUrl : sqs.listQueues().getQueueUrls()) {
    System.out.println("  QueueUrl: " + queueUrl);
}
System.out.println();
```

### Receiving and deleting a message from an Amazon SQS queue
![lifecycle](images/sqs-message-lifecycle-diagram.png)
- Amazon SQS doesn't automatically delete a message after receiving it for you
- To delete a message, you must send a separate request which acknowledges that you no longer need the message because you've successfully received and processed it. 
- Note that you must receive a message before you can delete it.

```java
// Receive messages
System.out.println("Receiving messages from MyQueue.\n");
final ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(myQueueUrl);
final List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
for (final Message message : messages) {
    System.out.println("Message");
    System.out.println("  MessageId:     " + message.getMessageId());
    System.out.println("  ReceiptHandle: " + message.getReceiptHandle());
    System.out.println("  MD5OfBody:     " + message.getMD5OfBody());
    System.out.println("  Body:          " + message.getBody());
    for (final Entry<String, String> entry : message.getAttributes().entrySet()) {
        System.out.println("Attribute");
        System.out.println("  Name:  " + entry.getKey());
        System.out.println("  Value: " + entry.getValue());
    }
}
System.out.println();
```

### Deleting an Amazon SQS queue
- AWS web console
    - From the queue list, select a queue.
    - From Queue Actions, select Delete Queue.
- AWS JAVA SDK
```java
// Delete the queue
System.out.println("Deleting the test queue.\n");
sqs.deleteQueue(new DeleteQueueRequest(myQueueUrl));
```

### [Subscribing an Amazon SQS queue to an Amazon SNS topic](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-subscribe-queue-sns-topic.html)