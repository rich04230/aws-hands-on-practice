# Containers on AWS (ECS)

Technical Requirements
- EC2 Container Registry (ECR)
- Elastic Container Service (ECS)
- Application Load Balancer (ALB)
	- Application Load Balancer is best suited for load balancing of HTTP and HTTPS traffic and provides advanced request routing targeted at the delivery of modern application architectures, including microservices and containers. 
	- Operating at the individual request level (Layer 7), Application Load Balancer routes traffic to targets within Amazon Virtual Private Cloud (Amazon VPC) based on the content of the request.

## [ECR](https://docs.aws.amazon.com/AmazonECR/latest/userguide/what-is-ecr.html) (Amazon Elastic Container Registry)

- ECR allows you to keep your images in a private registry called a repository. 
- ECR is fully compatible with the Docker CLI but also integrates deeply with the remaining ECS services.

- generate the CloudFormation template and create our stack as follows:

```bash
python ecr-repository-cf-template.py > ecr-repository-cf.template

# create stack
aws cloudformation create-stack \
--stack-name helloworld-ecr \
--capabilities CAPABILITY_IAM \
--template-body file://ecr-repository-cf.template \
--parameters ParameterKey=RepoName,ParameterValue=helloworld
```

```json
{
    "StackId": "arn:aws:cloudformation:us-east-1:635057350050:stack/helloworld-ecr/f274d820-770f-11ea-bd68-125f4521752f"
}
```

- After a few minutes, our stack will be created. We can validate that the
repository was correctly created as follows:

aws ecr describe-repositories
```json
{
    "repositories": [
        {
            "repositoryArn": "arn:aws:ecr:us-east-1:635057350050:repository/helloworld",
            "registryId": "635057350050",
            "repositoryName": "helloworld",
            "repositoryUri": "635057350050.dkr.ecr.us-east-1.amazonaws.com/helloworld",
            "createdAt": "2020-04-05T15:35:00+08:00",
            "imageTagMutability": "MUTABLE",
            "imageScanningConfiguration": {
                "scanOnPush": false
            }
        }
    ]
}
```

- We can see our exported output

aws cloudformation list-exports

```json
{
    "Exports": [
        {
            "ExportingStackId": "arn:aws:cloudformation:us-east-1:635057350050:stack/helloworld-ecr/f274d820-770f-11ea-bd68-125f4521752f",
            "Name": "helloworld-repo",
            "Value": "helloworld"
        }
    ]
}
```

Our repository can now be used to store our helloworld image.
- The first step of that process is to log in to the ecr service.
- To authenticate Docker to an Amazon ECR registry with [get-login-password](https://docs.aws.amazon.com/cli/latest/reference/ecr/get-login-password.html)

example: 

```bash
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <aws_account_id>.dkr.ecr.<region>.amazonaws.com
```
```bash
aws ecr get-login-password \
    --region us-east-1 \
| docker login \
    --username AWS \
    --password-stdin 635057350050.dkr.ecr.us-east-1.amazonaws.com
```

- tag image with registry uri
```bash
docker tag helloworld:latest 635057350050.dkr.ecr.us-east-1.amazonaws.com/helloworld:latest
```

- push that image to our registry
```bash
docker push 635057350050.dkr.ecr.us-east-1.amazonaws.com/helloworld:latest
```
- we can validate that the new image is present in our registry
```bash
aws ecr describe-images --repository-name helloworld
```

## ECS(Elastic Container Service)
- ECS is a fully managed container orchestration service.
- ECS run a number of services called tasks.
- ECS service provides an orchestration layer.
	- is in charge of managing the life cycle of containers, including upgrading or downgrading and scaling your containers up or down.
	- distributes all containers for every service across all instances of the cluster optimally. 
	- exposes a discovery mechanism that interacts with other services such as ALB and ELB to register and deregister containers.

### create a new script to generate our ECS cluster

To create our stack, we need three parameters: the key-pair, the VPC ID, and the
subnets.

- To get the VPC ID and the subnet IDs:

```bash
aws ec2 describe-vpcs --query 'Vpcs[].VpcId'
[
    "vpc-cbdaddb1"
]

aws ec2 describe-subnets --query 'Subnets[].SubnetId'
[
    "subnet-5dd56f10",
    "subnet-cc2f11f2",
    "subnet-6803e80e",
    "subnet-eee514cf",
    "subnet-3b1eb335",
    "subnet-0caa5d53"
]
```

### create our ECS Cluster stack
we will aim for one ECS cluster per environment, starting with staging.

```bash
aws cloudformation create-stack \
--stack-name staging-cluster \
--capabilities CAPABILITY_IAM \
--template-body file://ecs-cluster-cf.template \
--parameters \
ParameterKey=KeyPair,ParameterValue=EffectiveDevOpsAWS \
ParameterKey=VpcId,ParameterValue=vpc-cbdaddb1 \
ParameterKey=PublicSubnet,ParameterValue=subnet-5dd56f10\\,subnet-cc2f11f2\\,subnet-6803e80e\\,subnet-eee514cf\\,subnet-3b1eb335\\,subnet-0caa5d53

{
    "StackId": "arn:aws:cloudformation:us-east-1:635057350050:stack/staging-cluster/859f6270-7722-11ea-9b86-123cb1b397a4"
}
```

### Creating an ALB

- ECS provides an orchestrator that takes care of allocating the containers across our Auto Scaling Group. 
- It also keeps track of which port each container uses and integrates with ALB so that our load balancer can correctly route the incoming traffic to all containers running a given service. 
- ECS supports both the ELB and ALB services but the ALB gives more flexibility when working with containers.

ref helloworld-ecs-alb-cf-template.py

ALB works through the intermediary of three different resources. 

1. the ALB resource and security group, which handles incoming connections.
2. the target groups, which are the resources used by the ECS clusters registered to those ALBs. 
3. Finally, in order to tie the two, we find the listener's resources.

```bash
python3 helloworld-ecs-alb-cf-template.py > helloworld-ecs-alb-cf.template

aws cloudformation create-stack \
--stack-name staging-alb \
--capabilities CAPABILITY_IAM \
--template-body file://helloworld-ecs-alb-cf.template

{
    "StackId": "arn:aws:cloudformation:us-east-1:635057350050:stack/staging-alb/a2cfc320-772d-11ea-a0d4-0a2f4587a347"
}
```

### creating an ECS hello world service
We have an ECS cluster and a load balancer ready to take on traffic on one side
and an ECR repository containing the image of our application on the other side.
We now need to tie the two together.

- In ECS, applications are defined by their task definitions. 
	- which repository to use to get our image
	- how much CPU and memory the application needs
	- system properties such as port mapping, environment variables, mount points

ref: helloworld-ecs-service-cf-template.py

```bash
# Install Amazon Web Access Control Subsystem
# The awacs library allows for easier creation of AWS Access Policy Language JSON by writing Python code awacsto describe the AWS policies. 
pip3 install awacs

python3 helloworld-ecs-service-cf-template.py > helloworld-ecs-service-cf.template

aws cloudformation create-stack \
--stack-name staging-helloworld-service \
--capabilities CAPABILITY_IAM \
--template-body file://helloworld-ecs-service-cf.template \
--parameters ParameterKey=Tag,ParameterValue=latest

{
    "StackId": "arn:aws:cloudformation:us-east-1:635057350050:stack/staging-helloworld-service/5990bc40-772e-11ea-a01a-12e346dd5d0c"
}
```

- check the output of the ALB stack to get the URL of our newly deployed application and test its output
```bash
aws cloudformation describe-stacks \
--stack-name staging-alb \
--query 'Stacks[0].Outputs'

[
    {
        "OutputKey": "TargetGroup",
        "OutputValue": "arn:aws:elasticloadbalancing:us-east-1:635057350050:targetgroup/stagi-Targe-69NK9TB05W49/efc1f9b4f1cab143",
        "Description": "TargetGroup",
        "ExportName": "staging-alb-helloworld-target-group"
    },
    {
        "OutputKey": "URL",
        "OutputValue": "http://stagi-LoadB-NZJ8WH5N88JN-1285296685.us-east-1.elb.amazonaws.com:3000",
        "Description": "Helloworld URL"
    }
]

curl http://stagi-LoadB-NZJ8WH5N88JN-1285296685.us-east-1.elb.amazonaws.com:3000
Hello World
```

### update ECS service
we can easily manually deploy new code to our staging, as follows:

1. Make the changes in the helloworld.js code
2. Log in to the ECR registry
```bash
aws ecr get-login-password \
    --region us-east-1 \
| docker login \
    --username AWS \
    --password-stdin 635057350050.dkr.ecr.us-east-1.amazonaws.com
```
3. Build your docker image
```bash
docker build -t helloworld
```
4. Pick a new unique tag, and use it to tag your image.
```bash
docker tag helloworld 635057350050.dkr.ecr.us-east-1.amazonaws.com/helloworld:rich
```
5. Push the image to the ECR repository
```bash
docker push 635057350050.dkr.ecr.us-east-1.amazonaws.com/helloworld:rich
```
6. Update the ECS service CloudFormation stack
```bash
aws cloudformation update-stack \
--stack-name staging-helloworld-service \
--capabilities CAPABILITY_IAM \
--template-body file://helloworld-ecs-service-cf.template \
--parameters ParameterKey=Tag,ParameterValue=rich
```
7. Check the outputs after it updates
```bash
curl http://stagi-LoadB-NZJ8WH5N88JN-1285296685.us-east-1.elb.amazonaws.com:3000
Hello World, Rich!
```