## [Elastic Load Balancing](http://aws.amazon.com/elasticloadbalancing/)
- Elastic Load Balancing automatically distributes incoming application traffic across multiple Amazon EC2 instances and multiple AZ. 
- It enables you to achieve greater levels of fault tolerance in your applications, seamlessly providing the required amount of load balancing capacity needed to distribute application traffic.
- ELB ensures that only healthy Amazon EC2 instances receive traffic by detecting unhealthy instances and rerouting traffic across the remaining healthy instances. 
- ELB offers integration with Auto Scaling to ensure that you have back-end capacity to meet varying levels of traffic levels without requiring manual intervention.
- ELB works with **VPC** to provide robust networking and security features. 
	- You can create an internal (non-Internet facing) load balancer to route traffic using private IP addresses within your virtual network. 
	- You can implement a multi-tiered architecture using internal and Internet-facing load balancers to route traffic between application tiers. 
	- With this multi-tier architecture, your application infrastructure can use private IP addresses and security groups, allowing you to expose only the Internet-facing tier with public IP addresses.</p>
- ELB provides integrated certificate management and SSL decryption, allowing you to centrally manage the SSL settings of the load balancer and offload CPU-intensive work from your instances.

### ELB supports the following types of load balancers
- Application Load Balancers
	- used to route HTTP/HTTPS (or Layer 7) traffic
- Network Load Balancers
	- used to route TCP (or Layer 4) traffic
- Classic Load Balancers.
	- either the transport layer (TCP/SSL) or the application layer (HTTP/HTTPS).

### Advanced health check settings
- The ELB will periodically test the ping path on each of your web service instances to determine health: a 200 HTTP response code indicates a healthy status, and any other response code indicates an unhealthy status. 
- If an instance is unhealthy and continues in that state for a successive number of checks (**unhealthy threshold**), the load balancer will remove it from service until it recovers.
- The **healthy threshold** is the number of successful checks the load balancer expects to see in a row before bringing an instance into service behind the load balancer. The lower value will speed things up for this exercise.

### View Elastic Load Balancing CloudWatch Metrics
- In this task, you will use Amazon CloudWatch to monitor your ELB. Elastic Load Balancing automatically reports load balancer metrics to CloudWatch.
- Load balancing metrics include latency, request count, and healthy and unhealthy host counts. Metrics are reported as they are encountered and can take several minutes to show up in CloudWatch.

