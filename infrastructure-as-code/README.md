# Managing your infrastructure with CloudFormation

- CloudFormation introduces a new way to manage services and their configurations. 
- Through the creation of JSON or YAML files, CloudFormation lets you describe exactly the AWS architecture you would look like to build. 
- Once your files are created, you can simply upload your files to CloudFormation, which will execute them and automatically create or update your AWS resources.
- Most AWS managed tools and services are supported. [ref](http://amzn.to/1Odslix)

## CloudFormation
- Stack
    - The service is organized around the concept of stacks. 
	- Each stack typically describes a set of AWS resources and their configuration in order to start an application.
- Template 
  AWS has a number of well-written examples available at [sample templates](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-sample-templates-us-east-1.html)
- access CloudFormation via the AWS console (https://console.aws.amazon.com/cloudformation) 
- or by using the command-line:
```bash
$ aws cloudformation help # for the list of options
```

- At the highest level templates are structured as follows:
```json
{
"AWSTemplateFormatVersion" : "version date",
"Description" : "Description string",
"Resources" : { },	//describes which AWS services will be instantiated and what their configurations are
"Parameters" : { },
"Mappings" : { },
"Conditions" : { },	//add conditional logic to your other sections
"Metadata" : { },	//add more arbitrary information to your resources
"Outputs" : { }		//extract and print out useful information based on the execution of your template
}
```

## AWS CloudFormation designer
- is a tool that lets you create and edit CloudFormation templates using a
graphic user interface. 
- Designer hides a lot of the complexity of editing a CloudFormation template using a standard text editor. 
- You can access it directly at https://console.aws.amazon.com/cloudformation/designer or in the CloudFormation dashboard after you click on Create Stack
- You simply drag and drop resources from the left-hand side menu into a
canvas. 
- Once your resources are added, you can then connect them to other resources using the small dots surrounding each resource icon.

## Using troposphere to create a Python script for our template
- [troposphere](https://github.com/cloudtools/troposphere) - Python library to create AWS CloudFormation descriptions

- installation
```bash
pip3 install troposphere
```

- importing a number of definitions from the troposphere module

```python
from troposphere import (
    Base64,
    ec2,
    GetAtt,
    Join,
    Output,
    Parameter,
    Ref,
    Template,
)
```

- the first thing we will do is initialize a Template variable
```python
t = Template()
```

- add the description as follow:
```python
t.set_description("Effective DevOps in AWS: HelloWorld web application")
```

- select which key pair to use in order to gain SSH access to the host
	- we are going to create a Parameter object and initialize it by providing:
		- an identifier
		- a description
		- a parameter type
	- In order for this parameter to exist in our final template, we will also use the add_paramter() function defined in the template class

```python
t.add_parameter(Parameter(
    "KeyPair",
    Description="Name of an existing EC2 KeyPair to SSH",
    Type="AWS::EC2::KeyPair::KeyName",
    ConstraintDescription="must be the name of an existing EC2 KeyPair.",
))
```

- create a security group() and open up SSH and tcp/3000 to the world
	- we will add that new resource using the add_resource() function

```python
t.add_resource(ec2.SecurityGroup(
    "SecurityGroup",
    GroupDescription="Allow SSH and TCP/{} access".format(ApplicationPort),
    SecurityGroupIngress=[
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="22",
            ToPort="22",
            CidrIp=PublicCidrIp,
        ),
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort=ApplicationPort,
            ToPort=ApplicationPort,
            CidrIp="0.0.0.0/0",
        ),
    ],
))
```

- create [UserData](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.helloworld-cf-template) to provide a set of commands to run once the virtual machine has spawned up
	- constraints: must be base64-encoded to be added to our API call

```python
ud = Base64(Join('\n', [
    "#!/bin/bash",
    "sudo yum install --enablerepo=epel -y nodejs",
    "wget http://bit.ly/2vESNuc -O /home/ec2-user/helloworld.js",
    "wget http://bit.ly/2vVvT18 -O /etc/init/helloworld.conf",
    "start helloworld"
]))
```

- create an EC2 instance. The creation of the instance requires providing: 

```python
t.add_resource(ec2.Instance(
    "instance",                                 # a name for identifying the resource
    ImageId="ami-0ff8a91507f77f867",            # an image ID
    InstanceType="t2.micro",                    # an instance type
    SecurityGroups=[Ref("SecurityGroup")],      # a security group
    KeyName=Ref("KeyPair"),                     # he keypair to use for the SSH access
    UserData=ud,                                # the user data
))
```
- producing the Outputs section of the template that gets populated when CloudFormation creates a stack
```python
t.add_output(Output(
    "InstancePublicIp",
    Description="Public IP of our instance.",
    Value=GetAtt("instance", "PublicIp"),
))

t.add_output(Output(
    "WebUrl",
    Description="Application endpoint",
    Value=Join("", [
        "http://", GetAtt("instance", "PublicDnsName"),
        ":", ApplicationPort
    ]),
))
```

- run our script and generate the CloudFormation template

```bash
python3 helloworld-cf-template.py > helloworld-cf.template
```

- Create Stack from CloudFormation web console
[image]
