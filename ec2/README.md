## Amazon Elastic Compute Cloud (Amazon EC2)
- EC2 is a web service that provides resizable compute capacity in the cloud. 
- It is designed to make web-scale cloud computing easier for developers.

### Tutorial
- Launch a EC2 instance with termination protection enabled
- Monitor Your EC2 instance


### Launch a EC2 instance

- Choose an Amazon Machine Image (AMI)
AMI provides the information required to launch an instance, which is a virtual server in the cloud. An AMI includes:
	- A template for the root volume for the instance (for example, an operating system or an application server with applications)
	- Launch permissions that control which AWS accounts can use the AMI to launch instances
	- A block device mapping that specifies the volumes to attach to the instance when it is launched

- Choose an Instance Type
Amazon EC2 provides a wide selection of instance types:
	- General Purpose: The most popular; used for web servers, development environments, etc.
	- Compute Optimized: Good for compute-intensive applications such as some scientific modeling or high-performance web servers.
	- Memory Optimized: Used for anything that needs memory-intensive applications, such as real-time big data analytics, or running Hadoop or Spark.
	- Accelerated Computing: Include additional hardware (GPUs, FPGAs) to provide massive amounts of parallel processing for tasks such as graphics processing.
	- Storage Optimized: Ideal for tasks that require huge amounts of storage, specifically with sequential read-writes, such as log processing.

- Configure Instance Details
	- Network: indicates which Virtual Private Cloud (VPC) you wish to launch the instance into. You can have multiple networks, such as different ones for development, testing and production.
	- Enable termination protection: A terminated instance cannot be started again. If you want to prevent the instance from being accidentally terminated, you can enable it.
	- Advanced Details - User Data: When you launch an instance, you can pass user data to the instance that can be used to perform common automated configuration tasks and even run scripts after the instance starts.
	```bash
		#!/bin/bash
		yum -y install httpd
		systemctl enable httpd
		systemctl start httpd
		echo '<html><h1>Hello From Your Web Server!</h1></html>' > /var/www/html/index.html
	```

- Add Storage: Amazon EC2 stores data on a network-attached virtual disk called Elastic Block Store(EBS)

- Add Tags: Tags enable you to categorize your AWS resources in different ways, for example, by purpose, owner, or environment.

- Configure Security Group: you will config the security group to permit web traffic on port 80.
	- In the left navigation pane, click Security Groups
	- Select Web Server security group
	- Click the Inbound
	- Edit onfigure: Type: HTTP and Source: Anywhere
	
### Retrieve the public DNS entries for both of your EC2 instances
- All Amazon EC2 instances are assigned two IP addresses at launch: a *private IP address* and a *public IP address* that are directly mapped to each other through Network Address Translation (NAT). Private IP addresses are only reachable from within the Amazon EC2 network. Public addresses are reachable from the Internet.
- Amazon EC2 also provides an *internal DNS name* and a *public DNS name* that map to the private and public IP addresses, respectively. 

### Monitor Your Instance
- Click the Status Check: you can quickly determine whether Amazon EC2 has detected any problems that might prevent your instances from running applications.
- Click the Monitoring tab: This tab displays CloudWatch metrics for your instance.
- In the Actions menu select Instance Settings -> Get System Log: The System Log displays the console output of the instance, which is a valuable tool for problem diagnosis.
- In the Actions menu select Instance Settings -> Get Instance Screenshot: This shows you what your Amazon EC2 instance console would look like if a screen were attached to it.

---

### Amazon EC2 Auto Scaling

Auto Scaling helps you ensure that you have the correct number of Amazon EC2 instances available to handle your application's workload. You create collections of EC2 instances, called Auto Scaling groups

- You can specify the **minimum number** of instances in each Auto Scaling group, and Auto Scaling ensures that your group never goes below this size.
- You can specify the **maximum number** of instances in each Auto Scaling group, and Auto Scaling ensures that your group never goes above this size.
- If you specify a **desired capacity**, Auto Scaling ensures that your group always has a fixed number of instances.
- If you specify **scaling policies**, then Auto Scaling will launch new instances or terminate existing instances when the demand on your application increases or decreases.
- Auto Scaling only launches new instances or terminates existing instances. It does not Stop or Start instances.

#### Auto Scaling Group

- Your EC2 instances are organized into **Auto Scaling groups** and are treated as a logical unit for the purposes of scaling and management.
- When you create an Auto Scaling group, you can specify its minimum, maximum and desired number of EC2 instances.

#### Launch Configuration
- Your Auto Scaling group uses a **Launch Configuration** as a template for launching new EC2 instances. 
- When you create a Launch Configuration, you can specify information such as the AMI ID, instance type, key pair, security groups, and block device mapping for your instances.

#### Scaling plans
**Scaling Plan** tells Auto Scaling when and how to scale. Types of plans are:
- **Maintain current instance levels at all times**: Auto Scaling performs a periodic health check on running instances within an Auto Scaling group. When Auto Scaling finds an unhealthy instance, it terminates that instance and launches a new one.</li>
- **Manual scaling**: Manual scaling is the most basic way to scale your resources. You specify a change in the maximum, minimum, or desired capacity of your Auto Scaling group. Auto Scaling then manages the process of creating or terminating instances to maintain the updated capacity.
- **Scale based on a schedule**: Sometimes you know exactly when you will need to increase or decrease the number of instances in your group, simply because that need arises on a predictable schedule. Scaling by schedule means that scaling actions are performed automatically as a function of time and date.
- **Scale based on demand**: Define parameters that control the Auto Scaling process. For example, you can create a policy that calls for enlarging your fleet of EC2 instances whenever the average CPU utilization rate stays above ninety percent for fifteen minutes. This is useful when you can define how you want to scale in response to changing conditions, but you don't know when those conditions will change. You can set up Auto Scaling to respond for you.




