## [Amazon Kinesis](https://docs.aws.amazon.com/kinesis/index.html)

### Amazon Kinesis Data Streams
- Amazon Kinesis Data Streams (KDS) is a massively scalable and durable real-time data streaming service. 
- KDS can continuously capture gigabytes of data per second from hundreds of thousands of sources such as website clickstreams, database event streams, financial transactions, social media feeds, IT logs, and location-tracking events. 
- The data collected is available in milliseconds to enable real-time analytics use cases such as real-time dashboards, real-time anomaly detection, dynamic pricing

#### Determining the Initial Size of a Kinesis Data Stream
- Before you create a stream, you need to determine an initial size for the stream.
- To determine the initial size of a stream, you need the following input values:
    - The average size of the data record written to the stream in KB (average_data_size_in_KB)
    - The number of data records written to and read from the stream per second (records_per_second)
    - The number of Kinesis Data Streams applications that consume data concurrently and independently from the stream, that is, the consumers (number_of_consumers).
    - The incoming write bandwidth in KB (incoming_write_bandwidth_in_KB), which is equal to the average_data_size_in_KB multiplied by the records_per_second.
    - The outgoing read bandwidth in KB (outgoing_read_bandwidth_in_KB), which is equal to the incoming_write_bandwidth_in_KB multiplied by the number_of_consumers.
- You can calculate the initial number of shards (number_of_shards) that your stream needs by using the input values in the following formula:
```bash
number_of_shards = max(incoming_write_bandwidth_in_KiB/1024, outgoing_read_bandwidth_in_KiB/2048)
```

### Creates a Kinesis data stream with AWS CLI
```bash
aws kinesis create-stream \
    --stream-name samplestream \
    --shard-count 3
```

### Amazon Kinesis Firehose
- **Amazon Kinesis Firehose** is a fully managed service that delivers real-time streaming data to destinations such as Amazon Simple Storage Service (Amazon S3), Amazon Elasticsearch Service and Amazon Redshift. 
- With Firehose, you do not need to write any applications or manage any resources. You configure your data producers to send data to Firehose and it automatically delivers the data to the specified destination.

#### In this lab you will send simulated stock price data to Amazon Kinesis Firehose for capture and processing. The flow is:
![firehose-elk](images/kinesis-firehose.png)
- Generate streaming data containing stock quote information
- Send the data to an Amazon Kinesis Firehose delivery stream
- Amazon Kinesis Firehose will then call an AWS Lambda function to <em>transform</em> the data
- Amazon Kinesis Firehose will then collect the data into batches and send the batches to an Amazon Elasticsearch service cluster
- You will use Kibana to visualize the streaming data stored in the Elasticsearch cluster
#### Create your Amazon Kinesis Firehose
- In this task, you will create an Amazon Kinesis Firehose delivery stream. 
- It will *transform* incoming data by using the AWS Lambda function you just created and will then send the output to Elasticsearch. 
- An Amazon Elasticsearch Service cluster has been created for you automatically as part of the setup of this lab.