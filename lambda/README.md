## AWS Lambda
AWS Lambda is a compute service that runs your code in response to events and automatically manages the compute resources for you, making it easy to build applications that respond quickly to new information. 

- AWS Lambda starts running your code within milliseconds of an event such as an image upload, in-app activity, website click, or output from a connected device. 
- You can also use AWS Lambda to create new back-end services where compute resources are automatically triggered based on custom requests.

### Create a serverless image thumbnail application with AWS Lambda and S3

The following diagram illustrates the application flow:

![lambda-s3](images/lambda-s3.png)
- A user uploads an object to the source bucket in **Amazon S3** (object-created event).
- Amazon S3 detects the object-created event
- Amazon S3 publishes the object-created event to AWS Lambda by invoking the Lambda function and passing event data as a function parameter
- AWS Lambda executes the Lambda function.
- From the event data it receives, the Lambda function knows the source bucket name and object key name. The Lambda function reads the object and creates a thumbnail using graphics libraries, then saves the thumbnail to the target bucket.

- Tasks
	- Create the Amazon S3 Buckets: you will create two Amazon S3 buckets
	one for input(image-lambda-123) and one for output(image-lambda-123-resized)
	- Create an AWS Lambda Function: create an AWS Lambda function that reads an image from Amazon S3, resizes the image and then stores the new image in Amazon S3.
		- **Blueprints** are code templates for writing Lambda functions. 
		- Blueprints are provided for standard Lambda triggers such as creating Alexa skills and processing Amazon Kinesis Firehose streams.  
		- Author from scratch: a pre-written Lambda function from user, [function code](https://s3-us-west-2.amazonaws.com/us-west-2-aws-training/awsu-spl/spl-88/2.3.5.prod/scripts/CreateThumbnail.zip)

	```python
		import boto3
		import os
		import sys
		import uuid
		from PIL import Image
		import PIL.Image

		s3_client = boto3.client('s3')

		def resize_image(image_path, resized_path):
		    with Image.open(image_path) as image:
		        image.thumbnail((128, 128))
		        image.save(resized_path)

		def handler(event, context):
			# Receives an Event, which contains the name of the incoming object (Bucket, Key)
		    for record in event['Records']:
		        bucket = record['s3']['bucket']['name']
		        key = record['s3']['object']['key']
		        # Downloads the image to local storage
		        download_path = '/tmp/{}{}'.format(uuid.uuid4(), key)
		        upload_path = '/tmp/resized-{}'.format(key)

		        s3_client.download_file(bucket, key, download_path)
		        # Resizes the image using the Pillow library
		        resize_image(download_path, upload_path)
		        # Uploads the resized image to the -resized bucket
		        s3_client.upload_file(upload_path, '{}-resized'.format(bucket), key)
	```
	
	![lambda-trigger](images/create-thumbnail-button.png)
	- Create Trigger: AWS Lambda functions can be **triggered** automatically by activities such as data being received by Amazon Kinesis or data being updated in an Amazon DynamoDB database. 
		- select a trigger: S3
		- select your input bucket: (image-lambda-123)
		- event type: All object create events
	- Test Your Function
	- Monitoring and Logging (Click the **Monitoring** tab)
		- Invocations: The number of times the function has been invoked.
		- Duration: How long the function took to execute (in milliseconds).
		- Errors: How many times the function failed.
		- Throttles: When too many functions are invoked simultaneously, they will be throttled. The default is 1000 concurrent executions.
		- Iterator Age: Measures the age of the last record processed from streaming triggers (Amazon Kinesis and Amazon DynamoDB Streams).
		- Dead Letter Errors: Failures when sending messages to the Dead Letter Queue.