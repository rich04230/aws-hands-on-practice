## Amazon Relational Database Service (Amazon RDS) 

- Amazon RDS makes it easy to set up, operate, and scale a relational database in the cloud. It provides cost-efficient and resizable capacity while automating time-consuming administration tasks such as hardware provisioning, database setup, patching and backups. 
- Amazon RDS provides you with 6 database engines:
	- Amazon Aurora
	- PostgreSQL
	- MySQL
	- MariaDB
	- Oracle Database
	- SQL Server

- You can use the AWS Database Migration Service to easily migrate or replicate your existing databases to Amazon RDS.

### Create an RDS Instance
![create db](images/rdb-create.png)
![create db](images/rdb-config.png)
Click Create database then configure:
- DB type
	- Engine type: MySQL
	- Templates: Dev/Test
- DB instance identifier:
	- Master username & password
- DB instance size
	- Standard
	- Memory optimized
	- Burstable classes
- Connectivity
	- Publicly accessible: No
	- select VPC
	- VPC security groups

### Access the Database
![create db](images/rdb-status.png)
- Login to Your EC2 Instance
- You will now connect to the RDS database by using the **mysql** client installed on the EC2 instance.
	- in RDS dashboard click your db instance
	- Under the **Connectivity security** section, copy the **Endpoint**
```bash
mysql --user student --password --host lab-db.cspxs25wtmvm.us-west-2.rds.amazonaws.com
```

### Test SQL command
```bash
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| innodb             |
| lab                |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
6 rows in set (0.00 sec)
```

```bash
CREATE TABLE lab.staff (firstname text, lastname text, phone text);
INSERT INTO lab.staff VALUES ("John", "Smith", "555-1234");
INSERT INTO lab.staff VALUES ("Sarah", "Jones", "555-8866");

SELECT * FROM lab.staff WHERE firstname = "Sarah";
```

## Amazon Aurora 

Aurora is a MySQL and PostgreSQL-compatible relational database built for the cloud, that combines the performance and availability of traditional enterprise databases with the simplicity and cost-effectiveness of open source databases.

- Amazon Aurora is fully managed by Amazon Relational Database Service (RDS)
- Amazon Aurora features a distributed, fault-tolerant, self-healing storage system that auto-scales up to 64TB per database instance. 
- It delivers high performance and availability with up to 15 low-latency read replicas, point-in-time recovery, continuous backup to Amazon S3, and replication across three Availability Zones (AZs).

![create db](images/Aurora.png)

## Amazon ElastiCache
- Amazon ElastiCache allows you to seamlessly set up, run, and scale popular open-Source compatible in-memory data stores in the cloud. 
- Build data-intensive apps or boost the performance of your existing databases by retrieving data from high throughput and low latency in-memory data stores.
- Support for two engines:
    - Redis
    - Memcached
- features:
    - Compatibility with the specific engine protocol. This means most of the client libraries will work with the respective engines they were built for - no additional changes or tweaking required.
    - Detailed monitoring statistics for the engine nodes at no extra cost via Amazon CloudWatch
    - Pay only for the resources you consume based on node hours used