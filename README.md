# AWS Hands On Practice

Get Hands-on Practice with AWS

## awscli tool
The AWS Command Line Interface.

- installation
```bash
brew install awscli
```

- config awscli
```bash
aws configure
AWS Access Key ID [None]: <your-access-key-id>
AWS Secret Access Key [None]: <Secret Access Key>

aws iam list-users
{
    "Users": [
        {
            "Path": "/",
            "UserName": "rich",
            "UserId": "AIDAZHXC6EWRAMWCBPCRU",
            "Arn": "arn:aws:iam::635057350050:user/rich",
            "CreateDate": "2020-04-01T03:46:16+00:00",
            "PasswordLastUsed": "2020-04-07T08:58:32+00:00"
        }
    ]
}
```

## Creating our first web server
Launching a virtual server requires having a certain amount of information ahead of time. We will use the aws ec2 run-instances command, but we need to supply it with:
- An AMI ID
- An instance type
- A security group
- An SSH key-pair


## AMI
- An Amazon Machine Image (AMI) is a package that contains, among other things, the root filesystem with the operating system (for example, Linux, Unix, or Windows) and additional software required to start up the system. To find the proper AMI, we will use the aws ec2 describe-images.

```bash
aws ec2 describe-images --filters "Name=description,Values=Amazon Linux AMI * x86_64 HVM GP2" \
--query 'Images[*].[CreateDate, Description, ImageId]' \
--output text | sort -k 1 | tail

None	Amazon Linux AMI 2017.09.1.20180115 x86_64 HVM GP2	ami-97785bed
None	Amazon Linux AMI 2017.09.1.20180307 x86_64 HVM GP2	ami-1853ac65
None	Amazon Linux AMI 2017.09.rc-0.20170913 x86_64 HVM GP2	ami-5e8c9625
None	Amazon Linux AMI 2018.03.0.20180412 x86_64 HVM GP2	ami-467ca739
None	Amazon Linux AMI 2018.03.0.20180508 x86_64 HVM GP2	ami-14c5486b
None	Amazon Linux AMI 2018.03.0.20180622 x86_64 HVM GP2	ami-cfe4b2b0
None	Amazon Linux AMI 2018.03.0.20180811 x86_64 HVM GP2	ami-07edebf3b6affeea1
None	Amazon Linux AMI 2018.03.0.20180811 x86_64 HVM GP2	ami-0ff8a91507f77f867
None	Amazon Linux AMI VPC NAT x86_64 HVM GP2	ami-303b1458
None	Amazon Linux AMI VPC NAT x86_64 HVM GP2	ami-6e9e4b06
```

- we will select the t2.micro instance type as it is eligible for the AWS free usage tier.
Instance type: t2.micro

## Security Group
- Security groups work a bit like firewalls. 
- All EC2 instances have a set of security groups assigned to them. 
- Each security group contains rules to allow traffic to flow inbound (ingress) and/or outbound (egress)

### VPC
- VPC stands for Virtual Private Cloud. Despite being in a Cloud environment, where the physical resources are shared by all AWS
customers, there is still a strong emphasis on security. 
- AWS segmented their virtual infrastructure using a concept of virtual private cloud
- The security groups which protect our EC2 instances are tied subnets which in turn are tied to the network that the VPC provide:

- find out our default virtual private cloud (VPC) ID.

```bash
richde-MacBook-Air:~ rich$ aws ec2 describe-vpcs
```

```json
{
    "Vpcs": [
        {
            "CidrBlock": "172.31.0.0/16",
            "DhcpOptionsId": "dopt-689c3c12",
            "State": "available",
            "VpcId": "vpc-cbdaddb1",
            "OwnerId": "635057350050",
            "InstanceTenancy": "default",
            "CidrBlockAssociationSet": [
                {
                    "AssociationId": "vpc-cidr-assoc-0dc6d661",
                    "CidrBlock": "172.31.0.0/16",
                    "CidrBlockState": {
                        "State": "associated"
                    }
                }
            ],
            "IsDefault": true
        }
    ]
}
```

### create our new security group

```bash
aws ec2 create-security-group \
--group-name HelloWorld \
--description "Hello World Demo" \
--vpc-id vpc-cbdaddb1

{
    "GroupId": "sg-07cb4242e3e5cf374"
}
```

- By default, security groups allow all outbound traffic from the instance; we just need to open up ssh (tcp/22) and tcp/3000 for inbound traffic

```bash
aws ec2 authorize-security-group-ingress \
--group-name HelloWorld \
--protocol tcp \
--port 22 \
--cidr 0.0.0.0/0

aws ec2 authorize-security-group-ingress \
--group-name HelloWorld \
--protocol tcp \
--port 3000 \
--cidr 0.0.0.0/0
```

- verify the change of group

```bash
aws ec2 describe-security-groups \
--group-names HelloWorld \
--output json
```

```json
{
    "SecurityGroups": [
        {
            "Description": "Hello World Demo",
            "GroupName": "HelloWorld",
            "IpPermissions": [
                {
                    "FromPort": 22,
                    "IpProtocol": "tcp",
                    "IpRanges": [
                        {
                            "CidrIp": "0.0.0.0/0"
                        }
                    ],
{
    "SecurityGroups": [
        {
            "Description": "Hello World Demo",
            "GroupName": "HelloWorld",
            "IpPermissions": [
                {
                    "FromPort": 22,
                    "IpProtocol": "tcp",
                    "IpRanges": [
                        {
                            "CidrIp": "0.0.0.0/0"
                        }
                    ],
                    "Ipv6Ranges": [],
                    "PrefixListIds": [],
                    "ToPort": 22,
                    "UserIdGroupPairs": []
                },
                {
                    "FromPort": 3000,
                    "IpProtocol": "tcp",
                    "IpRanges": [
                        {
                            "CidrIp": "0.0.0.0/0"
                        }
                    ],
                    "Ipv6Ranges": [],
                    "PrefixListIds": [],
                    "ToPort": 3000,
                    "UserIdGroupPairs": []
                }
            ],
            "OwnerId": "635057350050",
            "GroupId": "sg-07cb4242e3e5cf374",
            "IpPermissionsEgress": [
                {
                    "IpProtocol": "-1",
                    "IpRanges": [
                        {
                            "CidrIp": "0.0.0.0/0"
                        }
                    ],
                    "Ipv6Ranges": [],
                    "PrefixListIds": [],
                    "UserIdGroupPairs": []
                }
            ],
            "VpcId": "vpc-cbdaddb1"
        }
    ]
}
```

## Generating your SSH keys

- By default, Amazon EC2 uses SSH key pairs to give you SSH access to your
EC2 instances

```bash
aws ec2 create-key-pair --key-name EffectiveDevOpsAWS \
--query 'KeyMaterial' --output text > ~/.ssh/EffectiveDevOpsAWS.pem

aws ec2 describe-key-pairs --key-name EffectiveDevOpsAWS
````

```json
{
    "KeyPairs": [
        {
            "KeyPairId": "key-0057a7d75537be86c",
            "KeyFingerprint": "40:1a:e7:27:fc:07:02:1c:0f:b0:c9:60:c8:62:5f:57:ac:a3:9d:66",
            "KeyName": "EffectiveDevOpsAWS",
            "Tags": []
        }
    ]
}
```

cat ~/.ssh/EffectiveDevOpsAWS.pem

- set read only permissions on your newly generated private (.pem) key file

```bash
```


## launch our first EC2 instance

```bash
aws ec2 run-instances \
--instance-type t2.micro \
--key-name EffectiveDevOpsAWS \
--security-group-ids sg-07cb4242e3e5cf374 \
--image-id ami-0ff8a91507f77f867
```

```json
{
    "Groups": [],
    "Instances": [
        {
            "AmiLaunchIndex": 0,
            "ImageId": "ami-0ff8a91507f77f867",
            "InstanceId": "i-0ca446767f59936b6",
            "InstanceType": "t2.micro",
            "KeyName": "EffectiveDevOpsAWS",
            "LaunchTime": "2020-04-02T13:59:36+00:00",
            "Monitoring": {
                "State": "disabled"
            },
            "Placement": {
                "AvailabilityZone": "us-east-1a",
                "GroupName": "",
                "Tenancy": "default"
            },
            "PrivateDnsName": "ip-172-31-94-196.ec2.internal",
            "PrivateIpAddress": "172.31.94.196",
            "ProductCodes": [],
            "PublicDnsName": "",
            "State": {
                "Code": 0,
                "Name": "pending"
            },
            "StateTransitionReason": "",
            "SubnetId": "subnet-eee514cf",
            "VpcId": "vpc-cbdaddb1",
            "Architecture": "x86_64",
            "BlockDeviceMappings": [],
            "ClientToken": "",
            "EbsOptimized": false,
            "Hypervisor": "xen",
            "NetworkInterfaces": [
                {
                    "Attachment": {
                        "AttachTime": "2020-04-02T13:59:36+00:00",
                        "AttachmentId": "eni-attach-0cea4fac65d9b9167",
                        "DeleteOnTermination": true,
                        "DeviceIndex": 0,
                        "Status": "attaching"
                    },
                    "Description": "",
                    "Groups": [
                        {
                            "GroupName": "HelloWorld",
                            "GroupId": "sg-07cb4242e3e5cf374"
                        }
                    ],
                    "Ipv6Addresses": [],
                    "MacAddress": "12:f0:83:e5:9a:d9",
                    "NetworkInterfaceId": "eni-004d5dfbf01e5ce32",
                    "OwnerId": "635057350050",
                    "PrivateDnsName": "ip-172-31-94-196.ec2.internal",
                    "PrivateIpAddress": "172.31.94.196",
                    "PrivateIpAddresses": [
                        {
                            "Primary": true,
                            "PrivateDnsName": "ip-172-31-94-196.ec2.internal",
                            "PrivateIpAddress": "172.31.94.196"
                        }
                    ],
                    "SourceDestCheck": true,
                    "Status": "in-use",
                    "SubnetId": "subnet-eee514cf",
                    "VpcId": "vpc-cbdaddb1",
                    "InterfaceType": "interface"
                }
            ],
            "RootDeviceName": "/dev/xvda",
            "RootDeviceType": "ebs",
            "SecurityGroups": [
                {
                    "GroupName": "HelloWorld",
                    "GroupId": "sg-07cb4242e3e5cf374"
                }
            ],
            "SourceDestCheck": true,
            "StateReason": {
                "Code": "pending",
                "Message": "pending"
            },
            "VirtualizationType": "hvm",
            "CpuOptions": {
                "CoreCount": 1,
                "ThreadsPerCore": 1
            },
            "CapacityReservationSpecification": {
                "CapacityReservationPreference": "open"
            },
            "MetadataOptions": {
                "State": "pending",
                "HttpTokens": "optional",
                "HttpPutResponseHopLimit": 1,
                "HttpEndpoint": "enabled"
            }
        }
    ],
    "OwnerId": "635057350050",
    "ReservationId": "r-029de692aff103256"
}
```

- check the status of ec2 instance

```bash
aws ec2 describe-instance-status --instance-ids i-0ca446767f59936b6

aws ec2 describe-instance-status --instance-ids i-0ca446767f59936b6 \
--output text| grep -i SystemStatus

```
## Connect to EC2  Instance using SSH 

- get pulbic DNS name
- 
```bash
aws ec2 describe-instances \
 --instance-ids i-0ca446767f59936b6 \
 --query "Reservations[*].Instances[*].PublicDnsName"

 [
    [
        "ec2-54-227-121-111.compute-1.amazonaws.com"
    ]
]


ssh -i ~/.ssh/EffectiveDevOpsAWS.pem ec2-user@ec2-54-227-121-111.compute-1.amazonaws.com
```

## Running a Node.js Hello World application

```bash
sudo yum install --enablerepo=epel -y nodejs

wget https://raw.githubusercontent.com/yogeshraheja/Effective-DevOps-with-AWS/master/Chapter02/helloworld.js -O ~/helloworld.js

node helloworld.js

// open this in your browser at the following link: ec2-54-227-121-111.compute-1.amazonaws.com:3000

```

- terminate EC2 instance
```bash
aws ec2 terminate-instances --instance-ids i-0ca446767f59936b6
```

```json
{
    "TerminatingInstances": [
        {
            "CurrentState": {
                "Code": 32,
                "Name": "shutting-down"
            },
            "InstanceId": "i-0ca446767f59936b6",
            "PreviousState": {
                "Code": 16,
                "Name": "running"
            }
        }
    ]
}
```