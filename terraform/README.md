# Infrastructure as Code with Terraform

- [AWS Provider](https://www.terraform.io/docs/providers/aws/index.html)
- [Resource: aws_instance](https://www.terraform.io/docs/providers/aws/r/instance.html#tags)

- install terraform using Homebrew
```bash
brew install terraform

terraform -v
Terraform v0.12.24
```

- Shared Credentials file
You can use an AWS credentials file to specify your credentials. The default location is $HOME/.aws/credentials

- Environment variables
You can provide your credentials via the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
Note that setting your AWS credentials using either these (or legacy) environment variables will override the use of AWS_SHARED_CREDENTIALS_FILE

```bash
501 rich:terraform (master) $ cat ~/.aws/credentials
[default]
aws_access_key_id =<anaccesskey>
aws_secret_access_key =<asecretkey>
```
```bash
$ export AWS_ACCESS_KEY_ID="anaccesskey"
$ export AWS_SECRET_ACCESS_KEY="asecretkey"
$ export AWS_DEFAULT_REGION="us-west-2"
```