provider "aws" {
  region = "us-east-1"
}

# Resource Configuration for AWS
resource "aws_instance" "myserver" {
  ami = "ami-0ff8a91507f77f867"
  instance_type = "t2.micro"
  key_name = "EffectiveDevOpsAWS2"
  vpc_security_group_ids = ["sg-07cb4242e3e5cf374"]

  tags = {
    Name = "HelloWorld"
  }
}